Build an app that does the following:

using this API: https://next.json-generator.com/api/json/get/Nk63L1Weu

- Load a list of data to a recyclerview (https://guides.codepath.com/android/using-the-recyclerview)
- show be able to pull down to reload list and add have endless scrolling
- allow a user to like/unlike a post

Learning Material
===================================
Android Application Lifecycle - https://developer.android.com/guide/components/activities/activity-lifecycle
UI 101 - https://developer.android.com/training/basics/firstapp/building-ui
Android Essentials - https://developer.android.com/guide